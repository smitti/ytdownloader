# ytdownloader
# mit flock schaun dos es nur 1x laft
# de comments uncommentn --> download(), getTitleFromYT(), main --> Cleanup()
import os
import os.path
import shutil
import re
import subprocess
import logging


def DoInit():
    global LINKFILE
    global REGEXYTID
    global LOGFILEPATH
    LOGFILEPATH = "D:\\tmp\\yt\\downloaderLog.txt"
    LINKFILE = "D:\\tmp\\yt\\collLinks.txt"
    ###!!!LINKFILE ="/home/ned/test/py/tmp/data/collLinks.txt"
    ###!!!LOGFILEPATH="/home/ned/test/py/tmp/logs/downloaderLog.txt"
    REGEXYTID = "(be\/.*$|\?v=.*$)"

    # configuraion of logger
    logging.basicConfig(filename=LOGFILEPATH, level=logging.DEBUG,
                        format='%(asctime)s:%(levelname)s - %(lineno)d: %(message)s')


def TestPrint(string):
    l = list()
    l.append(string)
    print("TESTPRINT:")
    print(l)


def ReplaceWinUnspoortedChars(string):
    return string.replace("<", "").replace(">", "").replace("|", "").replace("\"", "").replace("?", "").replace("*",
                                                                                                                "").replace(
        ":", "").replace("\\", "").replace("/", "")


def GetFileContent():
    linkfiletmp = LINKFILE + ".tmp"
    content = list()

    if (os.path.isfile(LINKFILE)):
        try:
            shutil.move(LINKFILE, linkfiletmp)

        except (OSError, IOError):
            print("FILE IS NOT ACCESSIBLE!!!")
            pass

    if (os.path.isfile(linkfiletmp)):
        with open(linkfiletmp,
                  'r') as f:  # open the file within a "with" statement --> meaning the file is closed autom. when the statement is finished but also when an exception appears
            content = f.readlines()
            f.close()

    return content


def CleanUpLinkFile():
    linkfiletmp = LINKFILE + ".tmp"
    print("CLEANUP" + linkfiletmp)
    if (os.path.isfile(linkfiletmp)):
        try:
            os.remove(linkfiletmp)

        except (OSError, IOError):
            print("File Deletion Error. Not accessible or not existing")
            pass


def GetTitleFromYT(ytid):
    res = ""
    print("-->GETTITLEFROMYT from ytID:" + ytid)
    if (ytid is not None and ytid is not ""):
        res = "DEFARTIST DEFTITLE"
    ###answer = os.popen("/usr/local/bin/youtube-dl -e https://www.youtube.com/watch?v="+ytid)
    ###res = answer.read()
    return res


def SplitID(string):
    expr = re.compile(REGEXYTID)
    reResult = expr.search(string)

    id = reResult.group(0)
    return id[3:len(
        id)]  # 2 options which this might look like after regex: ?v=nntGTK2Fhb0	#be/p8pN6vbl3ug --> therefore split


def SplitArtistTitle(string, artTit):
    if (artTit == "artist"):
        # print("ARTTIT: "+string[2:len(string)].strip().split('-')[0].strip())
        if (string.strip()[0:2] == "-s"):  # if it is a parameter as link (with leading "-s")
            return string[2:len(string)].strip().split('-')[0].strip()  # first remove the -s for splitting by "-"
        elif ("-" in string):
            return string.strip().split('-')[0].strip()
        else:
            return "Artist"  # Default value if no artist is found

    elif (artTit == "title"):
        # print("ARTTIT: " + string[2:len(string)].strip().split('-')[1].strip())
        if ("-" in string):
            return string[2:len(string)].strip().split('-')[1].strip()  # first remove the -s for splitting by "-"
        else:
            return "Title"  # Default value if no title is found
    else:
        return ""


class Ytlink:
    TMPPATH = "D:\\tmp\\yt\\"
    MUSICPATH = "D:\\tmp\\yt\\musicworkdir\\"
    UPLFOLDER = "D:\\tmp\\yt\\final\\"

    ###!!!!TMPPATH="/home/ned/test/py/tmp/"
    ###!!!!MUSICPATH="/home/ned/test/py/tmp/musicworkdir/"
    ###!!!!UPLFOLDER="/home/ned/test/py/tmp/final/"


    def __init__(self, linkWithParameters, ytID=None, artist=None, title=None, cutTime=None, path=None):
        self.ytID = ytID
        self.artist = artist
        self.title = title
        self.cutTime = cutTime
        self.path = path
        self.linkWithParameters = linkWithParameters  # link as read from the file

    def printYtLink(self):
        print("ID: {0}, Artist/Title {1} - {2}, cut: {3}, path:{4}".format(self.ytID, self.artist, self.title,
                                                                           self.cutTime, self.path))  #

    def download(self):
        print("-->DOWNLOAD: #################################")
        logging.debug("DOWNLOAD")
        tmpPath = self.TMPPATH + self.ytID + ".mp3"
        destPath = self.MUSICPATH + self.ytID + ".mp3"

        if (not os.path.isfile(tmpPath)):
            print('DOWNLOAD')  # erst wenn download fertig is donn solls in tmp sein
            subprocess.call(
                ['/usr/local/bin/youtube-dl', '-i', '-x', '--audio-format', 'mp3', '--format', 'bestaudio', '-o',
                 self.TMPPATH + '%(id)s.%(ext)s', 'https://www.youtube.com/watch?v=' + self.ytID])
        # check again after download
        if (os.path.isfile(tmpPath)):
            print("copy")
            try:
                shutil.copy(tmpPath, destPath)
                self.path = destPath

            except (OSError, IOError):
                print("COPY FILE ERROR!!!")
                pass
        else:
            logging.debug("Downloaded file not found!")

    def cut(self):
        if (self.cutTime is not None and self.isFile()):
            print("->CUT")
            # print("**command: mp3cut -t " + self.cutTime[3:len(self.cutTime)]+ " -o "+ self.path+".cut " + self.path)
            ###subprocess.call(['/usr/bin/mp3cut','-o' ,self.path+".cut", '-t',self.cutTime[3:len(self.cutTime)] , self.path])

            try:
                if (os.path.isfile(self.path) and os.path.isfile(self.path + ".cut")):
                    # input("Continue?: ")
                    os.remove(self.path)
                    # input("Continue?: ")
                    shutil.move(self.path + ".cut", self.path)

                if (os.path.isfile(self.path + ".cut") and not os.path.isfile(
                        self.path)):  # this is just the case if the script crashes betweeen downloading and cutting and there remains a .cut file
                    # input("CUT found but no orig. Continue?: ")
                    shutil.move(self.path + ".cut", self.path)

            except (OSError, IOError):
                print("File replace Error. Not accessible or not existing")
                pass

    def tag(self):
        print("ISFILE: " + self.path)
        print(self.isFile())
        if (self.isFile()):
            print("-->TAG")
            logging.debug("TAG")
            thumbnailPath = self.ThumbToTmp()
            if (thumbnailPath is not ""):
                ###subprocess.call(['/usr/bin/eyeD3','-a',self.artist, '-t', self.title, '-A',self.title, '--to-v2.3', '--add-image', thumbnailPath+":FRONT_COVER", self.path])
                # print("**command: THUMBNAIL-YES: subprocess.call(['/usr/bin/eyeD3','-a',self.artist, '-t', self.title, '-A',self.title, '--to-v2.3', '--add-image', thumbnailPath, self.path])")
                print("Thumbnail")
            else:
                ###subprocess.call(['/usr/bin/eyeD3','-a',self.artist, '-t', self.title, '-A',self.title, '--to-v2.3', self.path])
                # print("**command: THUMBNAIL-NO: subprocess.call(['/usr/bin/eyeD3','-a',self.artist, '-t', self.title, '-A',self.title, '--to-v2.3', self.path])")
                print("No Thumbnail")

            # eyeD3 -a $ARTIST -t $TITLE -A $TITLE --to-v2.3 --add-image $THUMBPATH:FRONT_COVER Dangerous\ \(Steve\ Aoki\ Remix\)\ -\ David\ Guetta.mp3
            # subprocess.call(['/usr/bin/eyeD3','-a',self.artist, '-t', self.title, '-A',self.title, '--to-v2.3',self.path])

            print(
                "**command: eyeD3 -a " + self.artist + " -t " + self.title + "--add-image:" + thumbnailPath + " " + self.path)
            try:
                newPath = self.MUSICPATH + ReplaceWinUnspoortedChars(self.artist + " - " + self.title + ".mp3")
                print("Move:", self.path, newPath)
                shutil.move(self.path, newPath)
                self.path = newPath
                print("-->RENAME")

            except (OSError, IOError):
                print("FILE IS NOT ACCESSIBLE!!!")
                pass

    def ThumbToTmp(self):
        toreturn = ""
        print("-->THUMBTOTMP")
        if (self.ytID is not None and self.ytID is not ""):
            answer = os.popen("/usr/local/bin/youtube-dl --get-thumbnail https://www.youtube.com/watch?v=" + self.ytID)
            linkToThumb = answer.read().replace(chr(10), "")  # remove the \n which is last char. of the reply
            print("RES: " + linkToThumb + "destination: " + self.TMPPATH + self.ytID + '.jpg')
            if (linkToThumb[0:4] == "http"):
                # wget https://i.ytimg.com/vi/4cLtiYvRvfE/maxresdefault.jpg -O /home/pic.test
                ###subprocess.call(['/usr/bin/wget',linkToThumb,'-O', self.TMPPATH+self.ytID+'.jpg'])
                logging.debug("WGET: /usr/bin/wget" + linkToThumb + "-O" + self.TMPPATH + self.ytID + '.jpg')
                toreturn = self.TMPPATH + self.ytID + ".jpg"

        return toreturn;

    def isFile(self):
        if (self.path is None or not os.path.isfile(self.path)):
            return False
        else:
            return True

    def CopyToULFolder(self):
        print("--> COPY TO UPLOAD")
        if (self.isFile()):
            try:
                shutil.move(self.path, self.UPLFOLDER + self.artist + " - " + self.title + ".mp3")

            except (OSError, IOError):
                print("FILE IS NOT ACCESSIBLE!!!")
                pass

    def chopLinkToAttributes(self):
        if (self.linkWithParameters):
            print("\n\n\n-------------LINK IS SET " + self.linkWithParameters)
            logging.debug("LINK IS SET " + self.linkWithParameters)
            linkparts = self.linkWithParameters.split(';')

            for part in linkparts:
                partHelp = part.replace("\n", "").strip()

                if "http" in partHelp[0:4]:
                    self.ytID = SplitID(partHelp)

                elif "-s" in partHelp[0:3]:
                    self.title = SplitArtistTitle(partHelp, "title")
                    self.artist = SplitArtistTitle(partHelp, "artist")

                elif "-t" in partHelp[0:3]:

                    self.cutTime = partHelp.strip()
        else:
            print("no link is set!")


# --- MAIN ---

DoInit()
logging.debug('DoInit() done')

cont = GetFileContent()
print(cont)

if cont:  # if there is a content, process it
    logging.debug("Cont is set!")
    print("cont is wos drin, passt")
    dict = {}
    # dict[test.artist+test.title] = test
    # print(dict[test.artist+test.title].ytID)

    for link in cont:

        ytLinkItem = Ytlink(link)
        logging.debug("traverse link: " + link)
        ytLinkItem.chopLinkToAttributes()
        logging.debug("Chop done")

        if (ytLinkItem.ytID is not None):
            if (ytLinkItem.artist is None or ytLinkItem.title is None):
                print("take original title for artist/title")
                ytLinkItem.artist = "ORIGINAL"
                ytLinkItem.title = "ORIGINAL"
                videoTitlefromYT = GetTitleFromYT(ytLinkItem.ytID)
                print("Returned Artist/Title:" + videoTitlefromYT)
                ytLinkItem.title = SplitArtistTitle(videoTitlefromYT, "title")
                ytLinkItem.artist = SplitArtistTitle(videoTitlefromYT, "artist")

            # --------just for test output
            print("----------Everything filled?\nID:" + ytLinkItem.ytID)
            if (ytLinkItem.artist is not None):
                print("ARTIST:" + ytLinkItem.artist)
            if (ytLinkItem.title is not None):
                print("TITLE:" + ytLinkItem.title)
            if (ytLinkItem.cutTime is not None):
                print("CUT:" + ytLinkItem.cutTime)
            ytLinkItem.printYtLink()
            print("----------")
            # -----------------------------
            logging.debug("Start Download")

            ytLinkItem.download()
            logging.debug("Start CuT")
            ytLinkItem.cut()
            logging.debug("Start tag")
            ytLinkItem.tag()
            ytLinkItem.printYtLink()
            ytLinkItem.CopyToULFolder()
            # !!!! UNCOMMENT: CleanUpLinkFile()

else:
    print("content nix drin")

# os.remove(linkfiletmp)

# datei moven von linklist.txt --> linklist.tmp auslesen
# für jeden link ein eigenes object in dict
# --> ID, Tags, time, path einfüllen

# dict durchlaufen
# downloaden wenn pfad nicht existiert (des in da download checkn)
# --> download in /tmp/ytID.mp3
# --> kopieren nach musik ordner
# --> self.path setzen (als flag das es jetzt im musik ordner ist u. gleich zum checken für isFile-())
# --> taggen (wenn self.artist/self.title gesetzt, dann das nehmen, sonst von yt)
# --> schneiden
# --> umbenennen


# youtube-dl -i -x --audio-format "mp3" --format "bestaudio" -o "/home/ned/Desktop/%(id)s.%(ext)s" https://www.youtube.com/watch?v=1l27kBb21Ds --> filkename is 1l27kBb21Ds.mp3

