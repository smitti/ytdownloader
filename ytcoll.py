# mailParser
# INFO: .replace(chr(10)," ") is meaning replace \n by " "

import sys
import email
import quopri
from email.parser import Parser
import chardet
import re
import logging
import os.path
import time


def DoInit():
    global OUTPUTFILE
    global REGEXSTRING
    global LOGFILEPATH
    OUTPUTFILE = "D:\\tmp\\yt\\collLinks.txt"
    LOGFILEPATH = "D:\\tmp\\yt\\parserLog.txt"
    # prev regex:
    # "(http|https):\/\/(www.youtube.com|youtu.be)(\/watch\?v=)?([^;|\s]+[;|\s])((-s*|-t*)([^;])*;?(-t\s?([\d:-])*|-s ([^;]))?;;)?" simple yt link without a ";" at the end does not work and if 2nd parm is -s it is not recognized
    # https://regex101.com/r/pB6zL0/11 oder https://regex101.com/r/pB6zL0/12
    REGEXSTRING = "(http|https):\/\/(www.youtube.com\/watch\?v=|youtu.be)([^;|\s]+[;|\S])((-s*|-t*)([^;])*;?((-s*|-t*)([^;])*)?;;)?"  # -> this one also finds youtube.com/channels/<ID>"(http|https):\/\/(www.youtube.com|youtu.be)(\/watch\?v=)?([^;|\s]+[;|\S])((-s*|-t*)([^;])*;?((-s*|-t*)([^;])*)?;;)?"

    # configuraion of logger
    logging.basicConfig(filename=LOGFILEPATH, level=logging.DEBUG,
                        format='%(asctime)s:%(levelname)s - %(lineno)d: %(message)s')


def GetMailBody(mess):
    messageBody = ""
    if mess.is_multipart():
        for payl in mess.get_payload():
            # print("CONTENT TYPE:",payl1.get_content_type()) #content_type = text/plain or text/html
            # print("CONTENT CHARSET:",payl1.get_content_charset()) #content_charset e.g. iso-8859-1
            if payl.get_content_type() == "text/plain":
                # print("PAYLOAD: ", payl.get_payload().replace(chr(10),"-\n\n"))
                messageBody += payl.get_payload()

    # print(quopri.decodestring(messageBody))
    return messageBody  # .decode("utf-8",errors="ignore")


def GetMailBodyAsList(mess):
    messageBody = list()
    if mess.is_multipart():
        for payl in mess.get_payload():
            if payl.get_content_type() == "text/plain":
                messageBody = payl.get_payload().split(chr(10))

    return messageBody


def DecodeAndRMNewLine(body):
    # =CB=83 is < from yt
    return quopri.decodestring(body).decode("UTF-8").replace(chr(10),
                                                             " ")  # decode quote-printable to UTF-8 and remove new-lines (not \n but character 10)


def AddToFile(lineToAdd):
    ''' Add a line to the outputfile by reading it into a set(no duplicates allowed), adding the lineToAdd to the set as well and writing the whole set back to the file
        At the end of each line a ";" is written and also a linebreak
    '''
    # will get filled later on if there is already a file
    cont = list()

    # check if lineToAdd is a string and not empty
    if isinstance(lineToAdd, str) and lineToAdd != "":
        logging.debug("FILEIO - LineToAdd is a string: " + lineToAdd)
        # print("LINE TO ADD: ",set(lineToAdd))
        # path of file to open

        if (os.path.isfile(OUTPUTFILE)):
            with open(OUTPUTFILE,
                      'r') as f:  # open the file within a "with" statement --> meaning the file is closed autom. when the statement is finished but also when an exception appears
                cont = f.readlines()

        # remove blanks in front and at the end of the string with "strip()" and attach \n to the string because the value in the set would also be "somelink\n"
        lineToAdd = lineToAdd.strip() + "\n"
        # convert the list "cont" to a set. In a set each value just exists once
        uniqueCont = set(cont)
        logging.debug("Previously in file:   " + ', '.join(uniqueCont).replace(chr(10), "\\n"))
        uniqueCont.add(lineToAdd)
        logging.debug("File after line add: " + ', '.join(uniqueCont).replace(chr(10), "\\n"))

        # print("\n\n\n")
        # print("CONTENT AFTER ADING LINE: ",uniqueCont)


        print("\n\n\n")
        with open(OUTPUTFILE,
                  'w') as f:  # open the file within a "with" statement --> meaning the file is closed autom. when the statement is finished but also when an exception appears
            for line in uniqueCont:
                if line != "\n":  # if there are some newline when manually editing ignore them
                    # print("WRITE TO FILE: %s\n" % line.replace('\n',""))
                    f.write("%s\n" % line.replace('\n',
                                                  ""))  # when writing the file always a \n needs to be written after each line because "ASDF\n" (in the middle of the file) is not similar to "ASDF"(at the end when there is no \n). To prevent double \n\n at the end remove first all \n and write just one after each line

    # check if lineToAdd is a list
    elif isinstance(lineToAdd, list):
        logging.debug("FILEIO - LineToAdd is a list: " + ', '.join(linksWithParm))

        if (os.path.isfile(OUTPUTFILE)):
            with open(OUTPUTFILE,
                      'r') as f:  # open the file within a "with" statement --> meaning the file is closed autom. when the statement is finished but also when an exception appears
                cont = f.readlines()

        # convert the list "cont" to a set. In a set each value just exists once
        uniqueCont = set(cont)
        logging.debug("Previously in file:   " + ', '.join(uniqueCont).replace(chr(10), "\\n"))
        for linkWithParm in lineToAdd:
            # remove blanks in front and at the end of the string with "strip()" and attach \n to the string because the value in the set would also be "somelink\n"
            linkWithParm = linkWithParm.strip() + "\n"
            uniqueCont.add(linkWithParm)

        logging.debug("File after line add: " + ', '.join(uniqueCont).replace(chr(10), "\\n"))

        print("\n\n\nSET TO SAVE")
        print(uniqueCont)
        with open(OUTPUTFILE,
                  'w') as f:  # open the file within a "with" statement --> meaning the file is closed autom. when the statement is finished but also when an exception appears
            for line in uniqueCont:
                if line != "\n":  # if there are some newline when manually editing ignore them
                    # print("WRITE TO FILE: %s\n" % line.replace('\n',""))
                    f.write("%s\n" % line.replace('\n',
                                                  ""))  # when writing the file always a \n needs to be written after each line because "ASDF\n" (in the middle of the file) is not similar to "ASDF"(at the end when there is no \n). To prevent double \n\n at the end remove first all \n and write just one after each line
                    # time.sleep(2.5) #for testing
                    # print("wrote to file") #for testing


def RegExBodyToList(body):
    expr = re.compile(REGEXSTRING)
    reResult = expr.search(body)

    # print("\n\nRERESULT:")
    # print(reResult)


    linklist = list()

    while reResult is not None:
        # print("group(0)")
        # print("--> FOUND:",reResult.group(0))
        logging.debug("Regex found, matching group: " + reResult.group(0))

        linklist.append(reResult.group(0))

        # print("groups()")
        # print(reResult.groups)

        # print("start/end")
        # print(reResult.start() , " ", reResult.end())

        bodyWithoutFound = body[0:reResult.start()] + body[reResult.end() + 1:len(body)]
        # print("\n\nbodyWihtoutFound: " + bodyWithoutFound)
        body = bodyWithoutFound
        reResult = expr.search(body)

    return linklist


# --- MAIN PART ---
DoInit()
logging.info("DoInit() done")

email_in = sys.stdin.read()
# print("email_in: ",email_in)
mparser = Parser()
mMail = mparser.parsestr(email_in)

body = GetMailBody(mMail)
body = DecodeAndRMNewLine(body)
logging.debug("BODY after parse Mail and decode: " + body)
# print(body)

# do the regex part and get the splitted download string in a list
linksWithParm = RegExBodyToList(body)

# print("\n\n\nLINKLIST: ")
# print(linksWithParm)

logging.debug('linksWithParm: ' + ', '.join(linksWithParm))

# check if the list has a content and is not [] or None
if linksWithParm:
    AddToFile(linksWithParm)
    logging.info("AddToFile() done ")






